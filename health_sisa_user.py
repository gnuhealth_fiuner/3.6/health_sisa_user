# -*- coding: utf-8 -*-
#This file is part health_sisa_user module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


__all__ = ['User']


class User(metaclass=PoolMeta):
    __name__ = "res.user"

    sisa_user = fields.Char('SISA User')
    sisa_password_hidden = fields.Char('SISA Password Hidden')
    sisa_password = fields.Function(fields.Char('SISA Password'),
        getter='get_sisa_password', setter='set_sisa_password')
    sisa_mode = fields.Selection([
        ('testing', 'Testing'),
        ('production', 'Production'),
        ], 'Mode', select=True)

    @staticmethod
    def default_sisa_mode():
        return 'testing'

    def get_sisa_password(self, name):
        return 'x' * 8

    @classmethod
    def set_sisa_password(cls, users, name, value):
        if value == 'x' * 8:
            return
        to_write = []
        for user in users:
            to_write.extend([[user], {
                'sisa_password_hidden': value,
                }])
        cls.write(*to_write)

    @classmethod
    def copy(cls, users, default=None):
        if default is None:
            default = {}
        default = default.copy()

        default['sisa_user'] = ''
        default['sisa_password'] = ''

        new_users = []
        for user in users:
            new_user, = super(User, cls).copy([user], default)
            new_users.append(new_user)
        return new_users
