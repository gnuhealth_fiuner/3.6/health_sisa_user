.. image:: https://www.gnuhealth.org/downloads/artwork/logos/isologo-gnu-health.png 


GNU Health HMIS: Libre Hospital Management and Health Information System
========================================================================

GNU Health / SISA: Sistema Integrado de Información Sanitaria Argentino
------------------------------------------------------------------------

Este módulo permite la integración de GNU Health con el Sistema Integrado
de Información Sanitaria Argentino (SISA).
Los servicios web brindados en SISA son de uso restringido, por lo cual
se requiere una cuenta de usuario y permisos de acceso, que deberán ser
gestionados ante las autoridades correspondientes.
Por más información, ver el siguiente enlace:
https://qa.sisa.msal.gov.ar/sisaqa/sisadoc/docs/0203/ws_intro.jsp

This module allows the integration of GNU Health with Argentine Integrated Health Information System (SISA).
The web services provided by SISA are use restricted, whereby a user account and access permissions are needed,
what should be managed by the corresponding authorities.
For more information, see the following link:
https://qa.sisa.msal.gov.ar/sisaqa/sisadoc/docs/0203/ws_intro.jsp

Documentation
-------------
* GNU Health
Wikibooks: https://en.wikibooks.org/wiki/GNU_Health/

Contact
-------
* GNU Health Contact 

 - website: https://www.gnuhealth.org
 - email: info@gnuhealth.org
 - Twitter: @gnuhealth

* FIUNER Contact 

 - email: saludpublica@ingenieria.uner.edu.ar
 
*  Silix
  website: http://www.silix.com.ar
  email: contacto@silix.com.ar

License
--------

GNU Health is licensed under GPL v3+::

 Copyright (C) 2008-2021 Luis Falcon <falcon@gnuhealth.org>
 Copyright (C) 2011-2021 GNU Solidario <health@gnusolidario.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


Prerequisites
-------------

 * Python 3.4 or later (http://www.python.org/)
 * Tryton 5.0 (http://www.tryton.org/)


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
